package HashMap;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Andrei",30,"Software Dev");
        Person person2 = new Person("Vlad",31,"Software Engineer");
        Person person3 = new Person("Alex",32,"Software Architect");
        Person person4 = new Person("Andreea",33,"Solutions Architect");

        HashMapImpl<String,Person> hashMap = new HashMapImpl<>();
        hashMap.put("p1",person1);
        hashMap.put("p2", person2);
        hashMap.put("p3", person3);
        hashMap.put("p3", person4);
        hashMap.remove("p1");
        System.out.println("Checking if hashmap contains p1: " + hashMap.containsKey("p1"));
        System.out.println("Checking if hashmap contains p2: " + hashMap.containsKey("p2"));
        System.out.println("Returning entry p1: " + hashMap.get("p1"));
        System.out.println("Returning entry p2: " + hashMap.get("p2"));
        System.out.println("Returning overwritten entry p3: " + hashMap.get("p3"));
        System.out.println("HashMapImpl size: " + hashMap.size);

        System.out.println("-----------------------------------");
        HashMapImpl<String,Integer> hashMap2 = new HashMapImpl<>();
        hashMap2.put("a",1);
        hashMap2.put("b",2);
        hashMap2.put("c",3);
        hashMap2.put("c",4);
        hashMap2.remove("a");
        System.out.println("Checking if hashmap contains p1: " + hashMap2.containsKey("a"));
        System.out.println("Checking if hashmap contains p2: " + hashMap2.containsKey("b"));
        System.out.println("Returning entry p1: " + hashMap2.get("a"));
        System.out.println("Returning entry p2: " + hashMap2.get("b"));
        System.out.println("Returning overwritten entry c: " + hashMap2.get("c"));
        System.out.println("HashMapImpl size: " + hashMap2.size);
    }
}
