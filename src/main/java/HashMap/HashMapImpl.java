package HashMap;

import java.util.LinkedList;

public class HashMapImpl<K,V> {
    K key;
    V value;
    LinkedList<Entry<K,V>>[] hashMap = new LinkedList[2];
    int size = 0;

    public HashMapImpl() {
    }

    public void put(K key, V value){
        if(size >= hashMap.length){
            resize();
        }

        int hashMapIndex = key.hashCode() % hashMap.length;

        if(hashMap[hashMapIndex] == null){
            hashMap[hashMapIndex] = new LinkedList<>();
            hashMap[hashMapIndex].add(new Entry<>(key, value));
            size++;
            return;
        }

        for(Entry<K,V> entry : hashMap[hashMapIndex]){
            if(entry.key.equals(key)){
                entry.value = value;
                size++;
                return;
            }
        }


        hashMap[hashMapIndex].add(new Entry<>(key, value));
        size++;
    }

    public V get(K key){
        int hashMapIndex = key.hashCode() % hashMap.length;

        if(hashMap[hashMapIndex] == null){
            return null;
        }

        for(Entry<K,V> entry : hashMap[hashMapIndex]){
            if(entry.key.equals(key)){
                return entry.value;
            }
        }

        return null;
    }

    public void remove(K key){
        if(key == null){
            return;
        }

        int hashMapIndex = key.hashCode() % hashMap.length;

        if(hashMap[hashMapIndex] == null){
            return;
        }

        Entry<K,V> toRemove = null;

        for(Entry<K,V> entry : hashMap[hashMapIndex]){
            if(entry.key.equals(key)){
                toRemove = entry;
                break;
            }
        }

        if(toRemove == null){
            return;
        }

        hashMap[hashMapIndex].remove(toRemove);
        size--;
    }

    public boolean containsKey(K key){
        if(key == null){
            return false;
        }

        int hashMapIndex = key.hashCode() % hashMap.length;

        if(hashMap[hashMapIndex] == null){
            return false;
        }

        for(Entry<K,V> entry : hashMap[hashMapIndex]){
            if(entry.key.equals(key)){
                return true;
            }
        }

        return false;
    }

    public int size(){
        return size;
    }

    public void resize(){
        LinkedList<Entry<K,V>>[] oldHashMap = hashMap;
        hashMap = new LinkedList[size * 2];
        size = 0;

        for(int i = 0; i < oldHashMap.length; i++){
            if(oldHashMap[i] != null){
                for(Entry<K,V> entry : oldHashMap[i]){
                    put(entry.key, entry.value);
                }
            }
        }
    }
}
